The MCmatlab project has been moved to GitHub:
https://github.com/ankrh/MCmatlab

Comments and questions can be directed to Anders Kragh Hansen, ankrh@fotonik.dtu.dk